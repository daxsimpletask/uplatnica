import { Injectable } from '@angular/core';
import { CurrencyService } from './currency.service';

@Injectable()
export class CurrencyModel {


    currencies = [];

    constructor(private service: CurrencyService) {
        this.service.getCurrenciesObservable().subscribe((currencies) => {
            this.currencies = currencies;
        });
    }

}