import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CurrencyService{

    constructor(private http:Http){}

    getCurrenciesObservable(){
        return this.http.get('https://openexchangerates.org/api/currencies.json').map((response)=>{
            var objekat = response.json();
            var result = [];
            for(var key in objekat){
                result.push({fullName:objekat[key],symbol:key});
            }
            return result;
        });
    }

}