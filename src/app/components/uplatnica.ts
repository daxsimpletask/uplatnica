import { Component } from '@angular/core';
import { CurrencyModel } from '../services/currency.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    templateUrl: './uplatnica.html',
})
export class UplatnicaComponent {

    uplatnicaForm: FormGroup;
    submitTried: boolean = false;

    constructor(private currenciesModel: CurrencyModel) {
        this.uplatnicaForm = new FormGroup({
            uplatilac: new FormGroup({
                ime: new FormControl('', Validators.required),
                prezime: new FormControl('', Validators.required),
                ulica: new FormControl('', Validators.required),
                broj: new FormControl('', Validators.required),
                postanskiBroj: new FormControl('', Validators.required),
                grad: new FormControl('', Validators.required)
            }),
            svrhaUplate: new FormControl('', Validators.required),
            primalac: new FormControl('', Validators.required),
            sifraPlacanja: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(3)]),
            valuta: new FormControl('', Validators.required),
            iznos: new FormControl('', Validators.required),
            racunPrimaoca: new FormControl('', [Validators.required, Validators.minLength(10)]),
            model: new FormControl(''),
            pozivNaBroj: new FormControl('', Validators.required),
            datumValute: new FormControl(new Date()),
            podpisUplatioca: new FormControl(false, Validators.requiredTrue)
        });
    }

    generatedValues = {};
    showGenerated = false;

    generisiUplatnicu() {
        this.submitTried = true;
        if (this.uplatnicaForm.valid) {
            var values = this.uplatnicaForm.value;
            this.generatedValues = {
                ...this.uplatnicaForm.value, 
                ja: (values.uplatilac.ime + " " 
                + values.uplatilac.prezime + "\n" 
                + values.uplatilac.ulica + " " 
                + values.uplatilac.broj+"\n"
                +values.uplatilac.postanskiBroj
                +" "+values.uplatilac.grad)}
                this.showGenerated = true;
            }
        }
    }
