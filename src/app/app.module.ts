import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { UplatnicaComponent } from './components/uplatnica';

import { CurrencyModel } from './services/currency.model';
import { CurrencyService } from './services/currency.service';

import { AppComponent } from './app.component';
const appRoutes: Routes = [
  {path:'', component:UplatnicaComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    UplatnicaComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule
  ],
  providers: [CurrencyModel,CurrencyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
